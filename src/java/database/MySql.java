/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import com.mysql.jdbc.Driver;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Nahum
 */
public class MySql {
    private Connection _connection;
    
    public MySql() {
        initialize();
    }
    
    public void initialize() {
        try {
            DriverManager.registerDriver(new Driver());
            _connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/lab03", "root", "");
        } catch (SQLException e) {
            System.out.println("Error al registrar el controlador" + e.getMessage());
        }
    }
    
    public Connection getConexion() {
        return _connection;
    }

    public void closeConexion() throws SQLException {
        _connection.close();
    }
}
