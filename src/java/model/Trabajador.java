/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Nahum
 */
public class Trabajador {

    private int idTrabajador;
    private String nombreTrabajador;
    private int edad;
    private String direccion;
    private int sueldoBase;
    private int sueldoHoraExtra;
    private String nombreFilial;

    public Trabajador(int idTrabajador, String nombreTrabajador, int edad, String direccion) {
        this.idTrabajador = idTrabajador;
        this.nombreTrabajador = nombreTrabajador;
        this.edad = edad;
        this.direccion = direccion;
    }

    public Trabajador(int idTrabajador, String nombreTrabajador, int edad, String direccion, int sueldoBase, int sueldoHoraExtra, String nombreFilial) {
        this.idTrabajador = idTrabajador;
        this.nombreTrabajador = nombreTrabajador;
        this.edad = edad;
        this.direccion = direccion;
        this.sueldoBase = sueldoBase;
        this.sueldoHoraExtra = sueldoHoraExtra;
        this.nombreFilial = nombreFilial;
    }

    public int getIdTrabajador() {
        return idTrabajador;
    }

    public void setIdTrabajador(int idTrabajador) {
        this.idTrabajador = idTrabajador;
    }

    public String getNombreTrabajador() {
        return nombreTrabajador;
    }

    public void setNombreTrabajador(String nombreTrabajador) {
        this.nombreTrabajador = nombreTrabajador;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getSueldoBase() {
        return sueldoBase;
    }

    public void setSueldoBase(int sueldoBase) {
        this.sueldoBase = sueldoBase;
    }

    public int getSueldoHoraExtra() {
        return sueldoHoraExtra;
    }

    public void setSueldoHoraExtra(int sueldoHoraExtra) {
        this.sueldoHoraExtra = sueldoHoraExtra;
    }

    public String getNombreFilial() {
        return nombreFilial;
    }

    public void setNombreFilial(String nombreFilial) {
        this.nombreFilial = nombreFilial;
    }

}
