/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Nahum
 */
public class Filial {

    private int idFilial;
    private String nombre;
    private String presidente;
    private String tipoFilial;

    public Filial(int idFilial, String nombre, String presidente, String tipoFilial) {
        this.idFilial = idFilial;
        this.nombre = nombre;
        this.presidente = presidente;
        this.tipoFilial = tipoFilial;
    }

    public int getIdFilial() {
        return idFilial;
    }

    public void setIdFilial(int idFilial) {
        this.idFilial = idFilial;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPresidente() {
        return presidente;
    }

    public void setPresidente(String presidente) {
        this.presidente = presidente;
    }

    public String getTipoFilial() {
        return tipoFilial;
    }

    public void setTipoFilial(String tipoFilial) {
        this.tipoFilial = tipoFilial;
    }

    

    


}
