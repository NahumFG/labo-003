/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Nahum
 */
public class Reporte {

    private String nombreFilial;
    private int horasTrabajadas;
    private int mes;
    private int anio;

    public Reporte(String nombreFilial, int horasTrabajadas, int mes, int anio) {
        this.nombreFilial = nombreFilial;
        this.horasTrabajadas = horasTrabajadas;
        this.mes = mes;
        this.anio = anio;
    }

    public String getNombreFilial() {
        return nombreFilial;
    }

    public void setNombreFilial(String nombreFilial) {
        this.nombreFilial = nombreFilial;
    }

    public int getHorasTrabajadas() {
        return horasTrabajadas;
    }

    public void setHorasTrabajadas(int horasTrabajadas) {
        this.horasTrabajadas = horasTrabajadas;
    }

    public String getMes() {
        String mesString = "";

        if (mes == 1) {
            mesString = "Enero";
        } else if (mes == 2) {
            mesString = "Febrero";
        } else if (mes == 3) {
            mesString = "Marzo";
        } else if (mes == 4) {
            mesString = "Abril";
        } else if (mes == 5) {
            mesString = "Mayo";
        } else if (mes == 6) {
            mesString = "Junio";
        } else if (mes == 7) {
            mesString = "Julio";
        } else if (mes == 8) {
            mesString = "Agosto";
        } else if (mes == 9) {
            mesString = "Setiembre";
        } else if (mes == 10) {
            mesString = "Octubre";
        } else if (mes == 11) {
            mesString = "Noviembre";
        } else if (mes == 12) {
            mesString = "Diciembre";
        }

        return mesString;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

}
