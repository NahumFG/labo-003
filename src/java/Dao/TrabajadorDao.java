/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import java.util.ArrayList;
import model.Trabajador;

/**
 *
 * @author Nahum
 */
public interface TrabajadorDao {
    public Trabajador getTrabajador(int idTrabajador);
    public ArrayList<Trabajador> getTrabajadores();
    public ArrayList<Trabajador> getTrabajadoresLista();
    public void insertTrabajador(String nombre, String edad, String direccion);
}
