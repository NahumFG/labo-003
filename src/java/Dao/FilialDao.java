/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import java.util.ArrayList;
import model.Filial;

/**
 *
 * @author Nahum
 */
public interface FilialDao {
    public ArrayList<Filial> getFiliares();
}
