/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao.impl;

import Dao.ReporteDao;
import database.MySql;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Reporte;


/**
 *
 * @author Nahum
 */
public class ReporteDaoImpl implements ReporteDao {

    private Connection conexion = new MySql().getConexion();
    private ArrayList<Reporte> arrayReporteAnual;

    @Override
    public ArrayList<Reporte> getReporte(int idTrabajador, int anio) {
        arrayReporteAnual = new ArrayList<>();
        Reporte reporte = null;

        try {
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery("SELECT filial.nombre, "
                    + "filial_has_trabajadores.horasTrab ,"
                    + "filial_has_trabajadores.mes, "
                    + "filial_has_trabajadores.anio "
                    + "FROM filial_has_trabajadores "
                    + "INNER JOIN filial "
                    + "ON filial_has_trabajadores.filial_idfilial = filial.idfilial "
                    + "INNER JOIN trabajadores "
                    + "ON filial_has_trabajadores.trabajadores_idtrabajadores = trabajadores.idtrabajadores "
                    + "WHERE trabajadores.idtrabajadores = " + idTrabajador + " "
                    + "AND filial_has_trabajadores.anio = " + anio + ";");

            while (rs.next()) {
                reporte = new Reporte(rs.getString(1), rs.getInt(2), rs.getInt(3), rs.getInt(4));
                arrayReporteAnual.add(reporte);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TrabajadorDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return arrayReporteAnual;
    }

}
