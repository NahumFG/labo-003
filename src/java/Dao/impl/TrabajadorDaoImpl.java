/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao.impl;

import Dao.TrabajadorDao;
import database.MySql;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Trabajador;

/**
 *
 * @author Nahum
 */
public class TrabajadorDaoImpl implements TrabajadorDao {

    private Connection conexion = new MySql().getConexion();
    private ArrayList<Trabajador> arrayListaTrabajadores;
    private ArrayList<Trabajador> arrayTrabajadores;

    @Override
    public Trabajador getTrabajador(int idTrabajador) {
        Trabajador trabajador = null;

        try {
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery("SELECT trabajadores.idTrabajadores, "
                    + "trabajadores.nombre, "
                    + "trabajadores.edad, "
                    + "trabajadores.direccion, "
                    + "contratos.sueldoBase,"
                    + "contratos.sueldoHoraExtra, "
                    + "filial.nombre "
                    + "FROM contratos "
                    + "INNER JOIN trabajadores "
                    + "ON contratos.trabajadores_idtrabajadores = trabajadores.idtrabajadores "
                    + "INNER JOIN filial "
                    + "ON contratos.filial_idfilial = filial.idfilial "
                    + "WHERE trabajadores.idtrabajadores = " + idTrabajador + ";");

            while (rs.next()) {
                trabajador = new Trabajador(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getInt(5), rs.getInt(6), rs.getString(7));
            }
        } catch (SQLException ex) {
            Logger.getLogger(TrabajadorDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return trabajador;
    }

    @Override
    public ArrayList<Trabajador> getTrabajadoresLista() {
        arrayListaTrabajadores = new ArrayList<>();
        Trabajador trabajador = null;

        try {
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery("SELECT trabajadores.idTrabajadores, trabajadores.nombre, trabajadores.edad, trabajadores.direccion, contratos.sueldoBase,contratos.sueldoHoraExtra, filial.nombre FROM contratos INNER JOIN trabajadores ON contratos.trabajadores_idtrabajadores = trabajadores.idtrabajadores INNER JOIN filial ON contratos.filial_idfilial = filial.idfilial;");

            while (rs.next()) {
                trabajador = new Trabajador(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getInt(5), rs.getInt(6), rs.getString(7));
                arrayListaTrabajadores.add(trabajador);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TrabajadorDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return arrayListaTrabajadores;
    }

    @Override
    public ArrayList<Trabajador> getTrabajadores() {
        arrayTrabajadores = new ArrayList<>();
        Trabajador trabajador = null;

        try {
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM lab03.trabajadores;");

            while (rs.next()) {
                trabajador = new Trabajador(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4));
                arrayTrabajadores.add(trabajador);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TrabajadorDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return arrayTrabajadores;
    }

    @Override
    public void insertTrabajador(String nombre, String edad, String direccion) {
        try {
            Statement st = conexion.createStatement();
            st.execute("INSERT INTO `lab03`.`trabajadores` (`nombre`, `edad`, `direccion`) VALUES ('" + nombre + "', '" + edad + "', '" + direccion + "');");

        } catch (SQLException ex) {
            Logger.getLogger(TrabajadorDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
