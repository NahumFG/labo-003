/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao.impl;

import Dao.FilialDao;
import database.MySql;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Filial;

/**
 *
 * @author Nahum
 */
public class FilialDaoImpl implements FilialDao {
    
    private Connection conexion = new MySql().getConexion();
    private ArrayList<Filial> arrayFiliales;
    @Override
    public ArrayList<Filial> getFiliares() {
        arrayFiliales = new ArrayList<>();
        Filial filial = null;
        
       try {
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery("SELECT filial.idfilial, "
                    + "filial.nombre, filial.presidente, "
                    + "tipo_filial.tipo FROM filial "
                    + "INNER JOIN tipo_filial "
                    + "ON filial.tipo_filial_idtipo_filial = tipo_filial.idTipo_filial;");

            while (rs.next()) {
                filial = new Filial(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
                arrayFiliales.add(filial);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FilialDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
       
       return arrayFiliales;
    }

}
