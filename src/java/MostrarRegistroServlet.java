

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import Dao.impl.ReporteDaoImpl;
import Dao.impl.TrabajadorDaoImpl;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Reporte;
import model.Trabajador;

/**
 *
 * @author Nahum
 */
@WebServlet(name = "MostrarRegistroServlet", urlPatterns = {"/MostrarRegistroServlet"})
public class MostrarRegistroServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet NewServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet NewServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);

        //Especificamos el formato de respuesta
        PrintWriter salida = response.getWriter();

        //Generar respuesta de la petición
        salida.print("<html><body>");
        salida.println("<h1 style='text-align:center'> Prueba Servlet - GET</h1>");
        salida.println("");
        salida.println("");
        salida.println("");

        salida.println("fecha y hora actual:" + new Date());
        salida.print("</html></body>");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter salida = response.getWriter()) {

            salida.print("<html><body>");
            salida.println("<h2 style='text-align:center'> Mostrar Historial - POST</h2>");
            salida.println("");
            salida.println("");
            salida.println("");

            int idUsuario = Integer.parseInt(request.getParameter("idUsuario"));
            int anio = Integer.parseInt(request.getParameter("anio"));

            ReporteDaoImpl reporteDaoImpl = new ReporteDaoImpl();
            ArrayList<Reporte> arrayReporteAnual = reporteDaoImpl.getReporte(idUsuario, anio);

            TrabajadorDaoImpl trabajadorDaoImpl = new TrabajadorDaoImpl();
            Trabajador trabajador = trabajadorDaoImpl.getTrabajador(idUsuario);

            if (arrayReporteAnual.isEmpty()) {
                salida.println("");
                salida.println("");
                salida.print("<h4 style='text-align:center'> No existen registros del año " + anio + "</h4>");
            } else {
                salida.print("<h4 style='text-align:center'> Historial del año: " + anio + "</h4>");
                salida.print("Nombre: " + trabajador.getNombreTrabajador());
                salida.print("<br>");
                salida.print("Filial Principal: " + trabajador.getNombreFilial());
                salida.print("<br>");
                salida.print("<br>");
                salida.print("<br>");

                salida.print("<table>");
                salida.print("<tr>");
                salida.print("<td><strong> NombreFilial</strong></td>");
                salida.print("<td><strong> HorasExtra</strong></td>");
                salida.print("<td><strong> Mes</strong></td>");
                salida.print("<td><strong> SueldoBase</strong></td>");
                salida.print("<td><strong> SueldoExtra</strong></td>");
                salida.print("<td><strong> SueldoTotal</strong></td>");

                salida.print("</tr>");

                int totalDePagos = 0;
                for (int i = 0; i < arrayReporteAnual.size(); i++) {
                    int sueldoBase = trabajador.getSueldoBase();
                    int sueldoExtra = trabajador.getSueldoHoraExtra() * arrayReporteAnual.get(i).getHorasTrabajadas();
                    salida.print("<tr>");
                    salida.print("<td>" + arrayReporteAnual.get(i).getNombreFilial() + "</td>");
                    salida.print("<td>" + arrayReporteAnual.get(i).getHorasTrabajadas() + "</td>");
                    salida.print("<td>" + arrayReporteAnual.get(i).getMes() + "</td>");
                    salida.print("<td>" + "S/. " + sueldoBase + "</td>");
                    salida.print("<td>" + "S/. " + sueldoExtra + "</td>");
                    salida.print("<td>" + "S/. " + (sueldoExtra + sueldoBase) + "</td>");
                    salida.print("</tr>");
                    totalDePagos += sueldoExtra + sueldoBase;
                }

                salida.print("</table>");
                salida.print("<h5>Total de pagos durante el año  S/. " + totalDePagos + " </h5>");
            }

            salida.print("</html></body>");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
