<%@page import="model.Trabajador"%>
<%@page import="Dao.impl.TrabajadorDaoImpl"%>
<%@page import="model.Filial"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Dao.impl.FilialDaoImpl"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" import="Dao.*" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Practica 1 - trabalhadores da corporação</title>
    </head>

    <body>
        <h1>Filiales</h1>
        <table>
            <tr>
                <td><strong>IdFilial   </strong></td>
                <td><strong>Nombre     </strong></td>
                <td><strong>Presidente </strong></td>
                <td><strong>TipoFilial </strong></td>
            </tr>

            <%
                FilialDaoImpl filialDaoImpl = new FilialDaoImpl();
                ArrayList<Filial> arrayFiliales = filialDaoImpl.getFiliares();

                for (int i = 0; i < arrayFiliales.size(); i++) {
                    out.print("<tr>");
                    out.print("<td>" + arrayFiliales.get(i).getIdFilial() + "</td>");
                    out.print("<td>" + arrayFiliales.get(i).getNombre() + "</td>");
                    out.print("<td>" + arrayFiliales.get(i).getPresidente() + "</td>");
                    out.print("<td>" + arrayFiliales.get(i).getTipoFilial() + "</td>");
                    out.print("</tr>");

                }

            %>

        </table>

        <h1>Trabajadores</h1>
        <table>
            <tr>
                <td><strong>idTrabajador   </strong></td>
                <td><strong>Nombre     </strong></td>
                <td><strong>Edad </strong></td>
                <td><strong>Dirección </strong></td>
            </tr>

            <%                TrabajadorDaoImpl trabajadorDaoImpl = new TrabajadorDaoImpl();
                ArrayList<Trabajador> arrayTrabajadores = trabajadorDaoImpl.getTrabajadores();

                for (int i = 0; i < arrayTrabajadores.size(); i++) {
                    out.print("<tr>");
                    out.print("<td>" + "7007174" + arrayTrabajadores.get(i).getIdTrabajador() + "</td>");
                    out.print("<td>" + arrayTrabajadores.get(i).getNombreTrabajador() + "</td>");
                    out.print("<td>" + arrayTrabajadores.get(i).getEdad() + "</td>");
                    out.print("<td>" + arrayTrabajadores.get(i).getDireccion() + "</td>");
                    out.print("</tr>");

                }
            %>

        </table>

        <h3>Agregar Trabajador - JSP</h3>

        <form action="Registro.jsp">
            Nombre: <input type="text" name="nombre">  
            Edad:   <input type="number" name ="edad"> 
            Direccion: <input type="text" name ="direccion"> 

            <input type="submit" value="Registrar">

        </form>

        <h3>Mostrar Historial - Servlet</h3>

        <form action="MostrarRegistroServlet" method="POST">

            <%
                out.println("Trabajador: <select name = 'idUsuario'>");
                for (int i = 0; i < arrayTrabajadores.size(); i++) {
                    Trabajador trabajador = arrayTrabajadores.get(i);
                    out.print("<option value = '" + trabajador.getIdTrabajador() + "'>" + trabajador.getNombreTrabajador() + "</option>");
                }
                out.println("</select>");
            %>
            Año: <select name ="anio">
                <option value="2018"> 2018 </option>
                <option value="2019"> 2019 </option>
            </select>
            
            <input type ="submit" value ="Seleccionar">
        </form>

    </body>
</html>
