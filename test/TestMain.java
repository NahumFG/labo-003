
import Dao.impl.FilialDaoImpl;
import Dao.impl.ReporteDaoImpl;
import Dao.impl.TrabajadorDaoImpl;
import java.util.ArrayList;
import model.Filial;
import model.Reporte;
import model.Trabajador;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Nahum
 */
public class TestMain {
    public static void main(String[] args) {
//        //Obtener Lista Filiales
//        FilialDaoImpl filialDaoImpl = new FilialDaoImpl();
//        ArrayList<Filial> arrayFiliales = filialDaoImpl.getFiliares();
//        
//        
//        //Mostrar datos de filiales
//        for (int i = 0; i < arrayFiliales.size(); i++) {
//            System.out.println(arrayFiliales.get(i).getIdFilial());
//            System.out.println(arrayFiliales.get(i).getNombre());
//            System.out.println(arrayFiliales.get(i).getPresidente());
//            System.out.println(arrayFiliales.get(i).getTipoFilial());
//            System.out.println("");
//        }
        
        //Obtener lista de trabajadores
        TrabajadorDaoImpl trabajadorDaoImpl = new TrabajadorDaoImpl();
//        ArrayList<Trabajador> arrayTrabajadores = trabajadorDaoImpl.getTrabajadoresLista();
        ArrayList<Trabajador> arrayTrabajadores = trabajadorDaoImpl.getTrabajadores();
        
        for (int i = 0; i < arrayTrabajadores.size(); i++) {
            System.out.println(arrayTrabajadores.get(i).getIdTrabajador());
            System.out.println(arrayTrabajadores.get(i).getNombreTrabajador());
            System.out.println(arrayTrabajadores.get(i).getEdad());
            System.out.println(arrayTrabajadores.get(i).getDireccion());
        }
        
        
        
        
//        //Mostrar datos de trabajadores
//        for (int i = 0; i < arrayTrabajadores.size(); i++) {
//            System.out.println(arrayTrabajadores.get(i).getIdTrabajador());
//            System.out.println(arrayTrabajadores.get(i).getNombreTrabajador());
//            System.out.println(arrayTrabajadores.get(i).getEdad());
//            System.out.println(arrayTrabajadores.get(i).getDireccion());
//            System.out.println(arrayTrabajadores.get(i).getSueldoBase());
//            System.out.println(arrayTrabajadores.get(i).getSueldoHoraExtra());
//            System.out.println(arrayTrabajadores.get(i).getNombreFilial());
//            System.out.println("");
//        }
        
        
        
        
//        //Obtener reporte
//        ReporteDaoImpl reporteDaoImpl = new ReporteDaoImpl();
//        ArrayList<Reporte> arrayReportes = reporteDaoImpl.getReporte(1, 2019);
//        
//        //Mostrar datos de reporte para el trabajador 1 del año 2019
//        for (int i = 0; i < arrayReportes.size(); i++) {
//            System.out.println(arrayReportes.get(i).getNombreFilial());
//            System.out.println(arrayReportes.get(i).getHorasTrabajadas());
//            System.out.println(arrayReportes.get(i).getMes());
//            System.out.println(arrayReportes.get(i).getAnio());
//            System.out.println("");
//        }
        
        
    }
}
