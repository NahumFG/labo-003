-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema lab03
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema lab03
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `lab03` DEFAULT CHARACTER SET utf8 ;
USE `lab03` ;

-- -----------------------------------------------------
-- Table `lab03`.`tipo_filial`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lab03`.`tipo_filial` (
  `idtipo_filial` INT NOT NULL AUTO_INCREMENT,
  `tipo` VARCHAR(45) NULL,
  PRIMARY KEY (`idtipo_filial`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lab03`.`filial`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lab03`.`filial` (
  `idfilial` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  `presidente` VARCHAR(45) NULL,
  `tipo_filial_idtipo_filial` INT NOT NULL,
  PRIMARY KEY (`idfilial`),
  INDEX `fk_filial_tipo_filial1_idx` (`tipo_filial_idtipo_filial` ASC) ,
  CONSTRAINT `fk_filial_tipo_filial1`
    FOREIGN KEY (`tipo_filial_idtipo_filial`)
    REFERENCES `lab03`.`tipo_filial` (`idtipo_filial`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lab03`.`trabajadores`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lab03`.`trabajadores` (
  `idtrabajadores` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  `edad` VARCHAR(45) NULL,
  `direccion` VARCHAR(45) NULL,
  PRIMARY KEY (`idtrabajadores`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lab03`.`contratos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lab03`.`contratos` (
  `idcontrato` INT NOT NULL AUTO_INCREMENT,
  `sueldoBase` VARCHAR(45) NULL,
  `sueldoHoraExtra` INT NULL,
  `trabajadores_idtrabajadores` INT NOT NULL,
  `filial_idfilial` INT NOT NULL,
  PRIMARY KEY (`idcontrato`),
  INDEX `fk_contratos_trabajadores1_idx` (`trabajadores_idtrabajadores` ASC) ,
  INDEX `fk_contratos_filial1_idx` (`filial_idfilial` ASC) ,
  CONSTRAINT `fk_contratos_trabajadores1`
    FOREIGN KEY (`trabajadores_idtrabajadores`)
    REFERENCES `lab03`.`trabajadores` (`idtrabajadores`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_contratos_filial1`
    FOREIGN KEY (`filial_idfilial`)
    REFERENCES `lab03`.`filial` (`idfilial`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lab03`.`filial_has_trabajadores`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lab03`.`filial_has_trabajadores` (
  `filial_idfilial` INT NOT NULL,
  `trabajadores_idtrabajadores` INT NOT NULL,
  `anio` INT NULL,
  `mes` INT NULL,
  `horasTrab` FLOAT NULL,
  INDEX `fk_table1_filial_idx` (`filial_idfilial` ASC) ,
  INDEX `fk_table1_trabajadores1_idx` (`trabajadores_idtrabajadores` ASC) ,
  CONSTRAINT `fk_table1_filial`
    FOREIGN KEY (`filial_idfilial`)
    REFERENCES `lab03`.`filial` (`idfilial`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_table1_trabajadores1`
    FOREIGN KEY (`trabajadores_idtrabajadores`)
    REFERENCES `lab03`.`trabajadores` (`idtrabajadores`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lab03`.`miembros_equipo_directivo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lab03`.`miembros_equipo_directivo` (
  `filial_idfilial` INT NOT NULL,
  `nombre` VARCHAR(45) NULL,
  INDEX `fk_equipo_directivo_filial1_idx` (`filial_idfilial` ASC) ,
  CONSTRAINT `fk_equipo_directivo_filial1`
    FOREIGN KEY (`filial_idfilial`)
    REFERENCES `lab03`.`filial` (`idfilial`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
