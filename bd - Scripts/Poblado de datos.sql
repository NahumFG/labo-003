INSERT INTO `lab03`.`tipo_filial` (`idtipo_filial`, `tipo`) VALUES ('1', 'Fabrica');
INSERT INTO `lab03`.`tipo_filial` (`idtipo_filial`, `tipo`) VALUES ('2', 'Ventas');
INSERT INTO `lab03`.`tipo_filial` (`idtipo_filial`, `tipo`) VALUES ('3', 'Distribucion');

INSERT INTO `lab03`.`filial` (`nombre`, `presidente`, `tipo_filial_idtipo_filial`) VALUES ('Filial 1', 'Daniel Angeles', '1');
INSERT INTO `lab03`.`filial` (`nombre`, `presidente`, `tipo_filial_idtipo_filial`) VALUES ('Filial 2 ', 'Nahum Flores Gutierrez', '2');
INSERT INTO `lab03`.`filial` (`nombre`, `presidente`, `tipo_filial_idtipo_filial`) VALUES ('Filial 3', 'José la Rosa', '3');

INSERT INTO miembros_equipo_directivo 
(filial_idfilial, nombre)
VALUES
(1,'Miembro 1 - 1'),
(1,'Miembro 1 - 2'),
(1,'Miembro 1 - 3'),

(2,'Miembro 2 - 1'),
(2,'Miembro 2 - 2'),
(2,'Miembro 2 - 3'),

(3,'Miembro 3 - 1'),
(3,'Miembro 3 - 2'),
(3,'Miembro 3 - 3');

INSERT INTO `lab03`.`trabajadores` (`nombre`, `edad`, `direccion`) VALUES ('Trabajador 1', '21', 'Direccion 1');
INSERT INTO `lab03`.`trabajadores` (`nombre`, `edad`, `direccion`) VALUES ('Trabajador 2', '22', 'Direccion 2');
INSERT INTO `lab03`.`trabajadores` (`nombre`, `edad`, `direccion`) VALUES ('Trabajador 3', '23', 'Direccion 3');

INSERT INTO `lab03`.`contratos` (`sueldoBase`, `sueldoHoraExtra`, `trabajadores_idtrabajadores`, `filial_idfilial`) VALUES ('1000', '10', '1', '1');
INSERT INTO `lab03`.`contratos` (`sueldoBase`, `sueldoHoraExtra`, `trabajadores_idtrabajadores`, `filial_idfilial`) VALUES ('1100', '20', '2', '2');
INSERT INTO `lab03`.`contratos` (`sueldoBase`, `sueldoHoraExtra`, `trabajadores_idtrabajadores`, `filial_idfilial`) VALUES ('1200', '30', '3', '3');

INSERT INTO filial_has_trabajadores 
(filial_idfilial, trabajadores_idtrabajadores, anio, mes, horasTrab)
VALUES
(1,1,2019,1,10),
(2,1,2019,2,11),
(3,1,2019,3,12),

(1,2,2019,1,21),
(2,2,2019,2,22),
(3,2,2019,3,23),

(1,3,2019,1,17),
(2,3,2019,2,18),
(3,3,2019,3,19);



